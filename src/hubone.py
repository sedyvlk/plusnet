#!/usr/bin/env python3.8
import hashlib
import os
import re
import time

import requests
from prometheus_client import Counter, Gauge, Summary, start_http_server
from pyquery import PyQuery

# Prometheus metrics
PROM_LABELS = [
    "site",
]
REQUEST_TIME_SUMMARY = Summary(
    "plusnet_request_processing_seconds", "Time spent processing request", PROM_LABELS
)
REQUEST_TIME = REQUEST_TIME_SUMMARY.labels(os.environ.get("PLUSNET_SITE"))

BROADBAND_UPTIME = Counter("plusnet_dsl_uptime", "Plusnet DSL uptime", PROM_LABELS)
DATA_UP = Counter("plusnet_data_sent", "Cumulative Data Upload", PROM_LABELS)
DATA_DOWN = Counter("plusnet_data_received", "Cumulative Data Download", PROM_LABELS)

DATARATE_UP = Gauge("plusnet_data_rate_up", "Plusnet Data rate Up", PROM_LABELS)
DATARATE_DOWN = Gauge("plusnet_data_rate_down", "Plusnet Data rate Down", PROM_LABELS)

MAX_DATARATE_UP = Gauge(
    "plusnet_max_data_rate_up", "Plusnet Max Data rate Up", PROM_LABELS
)
MAX_DATARATE_DOWN = Gauge(
    "plusnet_max_data_rate_down", "Plusnet Max Data rate Down", PROM_LABELS
)

NOISE_MARGIN_UP = Gauge(
    "plusnet_noise_margin_up", "Plusnet Noise Margin Up", PROM_LABELS
)
NOISE_MARGIN_DOWN = Gauge(
    "plusnet_noise_margin_down", "Plusnet Noise Margin Down", PROM_LABELS
)

LINE_ATTENUATION_UP = Gauge(
    "plusnet_line_attenuation_up", "Line attenuation Up", PROM_LABELS
)
LINE_ATTENUATION_DOWN = Gauge(
    "plusnet_line_attenuation_down", "Line attenuation Down", PROM_LABELS
)

SIGNAL_ATTENUATION_UP = Gauge(
    "plusnet_signal_attenuation_up", "Signal attenuation Up", PROM_LABELS
)
SIGNAL_ATTENUATION_DOWN = Gauge(
    "plusnet_signal_attenuation_down", "Signal attenuation Down", PROM_LABELS
)


def salted_password(admin_password, salt):
    """
    Similar to Hub one javascript code. The passwords is never sent over, instead it is salted
    and hashed
    :param admin_password: Router password
    :param salt: hub one provides salt value via hidden form element
    :return:
    """
    password = f"{admin_password}{salt}"
    return hashlib.md5(password.encode()).hexdigest()


def extract_login_info(content):
    """
    There are some values we need in order or submit the password. We extract them here, without much checking
    :param content: content of the login page
    :return: dictionary with extracted values
    """
    auth_key = re.search(r'name="auth_key" value="(\d+)"', content)
    post_token = re.search(r'name="post_token" value="(\w+)"', content)
    password_id = re.search(r'name="password_(\d+)" ', content)
    return dict(
        auth_key=auth_key.group(1),
        post_token=post_token.group(1),
        password_id=password_id.group(1),
    )


def parse_helpdesk_page(content):
    """
    Parse values from the website and return key, values
    :param content:
    :return:
    """
    pq = PyQuery(content)
    values = pq("td.bt_border").items()
    compiled_re = re.compile(r"(\d+)\.\s+(.+)")
    result_dict = dict()
    for value in values:
        key = value.text().strip(":").strip()
        match = compiled_re.match(key)
        order, key = match.groups()
        key_value = next(values).text().strip()
        result_dict[key] = key_value
    # One off match for uptime, there is javascript that keeps this ticking in the page
    match = re.search(r"wait = (\d+);", content)
    result_dict["DSL uptime"] = float(match.group(1))

    return result_dict


@REQUEST_TIME.time()
def hub_page(session, page_id, hub_ip, hub_password):
    """
    Some pages are protected, some are not.
    When we get redirected to the login pages we do that login dance, before returning the content,
    otherwise return the page's content right away
    :param page_id: pages have ids in plusnet world not names.
    :param hub_ip: hm what could this be
    :param hub_password: password
    :return: desired page content
    """
    result = session.get(f"http://{hub_ip}/index.cgi?active_page={page_id}")
    if protected := re.match(r"<!-- Page\((\d+)\)=\[Login\] -->", result.text):
        page_data = extract_login_info(result.text)
        md5_pass = salted_password(hub_password, page_data["auth_key"])
        data = {
            "md5_pass": md5_pass,
            "active_page": protected.group(1),
            "post_token": page_data["post_token"],
            f'password_{page_data["password_id"]}': "",
            "mimic_button_field": "submit_button_login_submit: ..",
            "auth_key": page_data["auth_key"],
        }
        result = session.post(f"http://{hub_ip}/index.cgi", data=data)
        result.raise_for_status()

    return result.text


def helpdesk_data_metrics(helpdesk_data, counters=None):
    """
    Dirty way of extracting and normalising what I need and making it available for prometheus to scrape
    :param helpdesk_data:
    :return:
    """
    # Filter out stuff we deffo do not need
    if counters is None:
        counters = dict()
    to_skip = {
        "Product name",
        "Serial number",
        "Firmware version",
        "Board version",
        "Broadband username",
        "2.4 GHz Wireless network/SSID",
        "2.4 GHz Wireless connections",
        "2.4 GHz Wireless security",
        "2.4 GHz Wireless channel",
        "5 GHz Wireless network/SSID",
        "5 GHz Wireless connections",
        "5 GHz Wireless security",
        "5 GHz Wireless channel",
        "Firewall",
        "MAC Address",
        "Modulation",
        "Modulation",
        "Software variant",
        "Boot loader",
    }
    for key, value in ((k, v) for k, v in helpdesk_data.items() if k not in to_skip):
        if key == "DSL uptime":
            BROADBAND_UPTIME.labels(os.environ.get("PLUSNET_SITE")).inc(
                max(value - counters.get("plusnet_dsl_uptime", 0), 0)
            )
            counters["plusnet_dsl_uptime"] = value
        elif key == "Data rate":
            up, down = map(float, (map(str.strip, value.split("/"))))
            DATARATE_UP.labels(os.environ.get("PLUSNET_SITE")).set(up)
            DATARATE_DOWN.labels(os.environ.get("PLUSNET_SITE")).set(down)
        elif key == "Maximum data rate":
            up, down = map(float, (map(str.strip, value.split("/"))))
            MAX_DATARATE_UP.labels(os.environ.get("PLUSNET_SITE")).set(up)
            MAX_DATARATE_DOWN.labels(os.environ.get("PLUSNET_SITE")).set(down)
        elif key == "Noise margin":
            up, down = map(float, (map(str.strip, value.split("/"))))
            NOISE_MARGIN_UP.labels(os.environ.get("PLUSNET_SITE")).set(up)
            NOISE_MARGIN_DOWN.labels(os.environ.get("PLUSNET_SITE")).set(down)
        elif key == "Line attenuation":
            up, down = map(float, (map(str.strip, value.split("/"))))
            LINE_ATTENUATION_UP.labels(os.environ.get("PLUSNET_SITE")).set(up)
            LINE_ATTENUATION_DOWN.labels(os.environ.get("PLUSNET_SITE")).set(down)
        elif key == "Signal attenuation":
            up, down = map(float, (map(str.strip, value.split("/"))))
            SIGNAL_ATTENUATION_UP.labels(os.environ.get("PLUSNET_SITE")).set(up)
            SIGNAL_ATTENUATION_DOWN.labels(os.environ.get("PLUSNET_SITE")).set(down)
        elif key == "Data sent/received":
            (up_value, up_unit), (down_value, down_unit) = map(
                str.split, map(str.strip, value.split("/"))
            )
            down_value = float(down_value)
            up_value = float(up_value)
            if up_unit == "GB":
                multiplier_up = 10 ** 9
            elif up_unit == "MB":
                multiplier_up = 10 ** 6
            else:
                raise ValueError(f"Unsupported units {up_unit}")
            if down_unit == "GB":
                multiplier_down = 10 ** 9
            elif down_unit == "MB":
                multiplier_down = 10 ** 6
            else:
                raise ValueError(f"Unsupported units {down_unit}")
            down_value *= multiplier_down
            up_value *= multiplier_up
            DATA_UP.labels(os.environ.get("PLUSNET_SITE")).inc(
                max(up_value - counters.get("data_sent", 0), 0)
            )
            DATA_DOWN.labels(os.environ.get("PLUSNET_SITE")).inc(
                max(down_value - counters.get("data_received", 0), 0)
            )
            counters["data_sent"] = up_value
            counters["data_received"] = down_value

    return counters


def main():
    start_http_server(9102)
    counters = None
    session = requests.session()
    while True:
        try:
            content = hub_page(
                session,
                9143,
                "192.168.1.254",
                os.environ.get("PLUSNET_HUB_PASSWORD")
            )
            if 'Page(9148)=[Login]' in content:
                raise PermissionError('Unable to login')

            data = parse_helpdesk_page(content)
            counters = helpdesk_data_metrics(data, counters)
        except requests.exceptions.ConnectionError as exp:
            # Hub is down, probably restarting or something
            print(exp, 'http Error, Will retry soon')
        except AttributeError as exp:
            print(exp, 'Parsing error, Will retry soon')
        time.sleep(900)


if __name__ == "__main__":
    main()
